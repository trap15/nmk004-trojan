# Using
The PC-side software consists of two parts. There's the main tool, named
**opnrom**, and a tool to generate trojan ROMs called **trogen**. This document
will cover setting up and using them both, as well as the complete procedure
for dumping the *NMK004's internal ROM*.

## opnrom
**opnrom** utilizes the **opncap-pc** library to parse dumps from
**opncap-fpga** and convert the data back to a binary and a *validity mask*.
The validity mask may be ignored for the most part, it is used internally. Here
is usage information, taken from the program itself:

        opnrom capture.cap outfile.bin outmask.bin baseadr
    
    baseaddr may be in any base, defaulting to base-16.

## trogen
**trogen** is a small tool used to create trojan ROMs to dump an *NMK004's
internal ROM* to the attached *OPN*'s PSG registers. Here is the usage
information, taken from the program itself:

        trogen trojan.bin baseaddr
    
    baseaddr may be in any base, defaulting to base-16.

The resulting binary should be burned to an EPROM, then replace the EPROM
closest to the *NMK004 IC*. You will usually want `baseaddr` to be 0. Here is
a list of compatible EPROM models:
 - 27C512
 - 27C256
 - 27C128
 - 27C64

If not using a *27C512*, simply trim the binary to fit. The binary mirrors
itself every 2048 bytes, so this should be no problem.

# Procedure
To perform the *NMK004* dump, you must generate a trojan ROM with **trogen**.
Once you have put the new EPROM in your board, attach **opncap-fpga** as
specified in its documentation. Turn on your board, and proceed to the sound
test menu.

Begin capturing on **opncap-fpga**, then play song ID 1. Once the song has
stopped, go to the next ID. Repeat this process until **opncap-fpga** is nearly
full. Then copy out the captured data, as specified in its documentation. Reset
**opncap-fpga**, then capture the next song. Continue this process until all
song IDs up to 0x1D have been dumped. That concludes the actual dumping, you
may now remove **opncap-fpga** from your game board.

To convert from the capture files back to the internal ROM, you must utilize
**reccat** from the ***OPNCAP*** project, and **opnrom** from this project.
First, combine all of your capture files with **reccat** as according to its
documentation. Then pass the output capture file to **opnrom**, with `baseadr`
set to 0. The resulting `outfile.bin` will be the dump.
