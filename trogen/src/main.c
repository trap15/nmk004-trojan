#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "xtoi.h"

#define ROM_BASE (0xE800)
#define ROM_SIZE (0x800)
#define STRIDE (0xC0)
#define SONG_COUNT (0x20)

uint8_t rom[ROM_SIZE];

enum {
  NOP_TBL_ADDR = 0xEF80,
  OKI_A_TBL = NOP_TBL_ADDR,
  OKI_B_TBL = NOP_TBL_ADDR,
  FM_MOD_TBL = NOP_TBL_ADDR,
  FM_PERIOD_TBL = NOP_TBL_ADDR,
  NOTE_SUBLEN_A_TBL = NOP_TBL_ADDR,
  NOTE_SUBLEN_B_TBL = NOP_TBL_ADDR,

  NOTE_LEN_TBL = 0xEF00, /* 2 bytes */
  PSG_NOTE_TBL = 0xEF02, /* 6 bytes */
  PSG_VOL_TBL = 0xEF08, /* SONG_COUNT * 3 * 2 bytes */
  CMD_TBL = 0xE800, /* SONG_COUNT * 25 bytes */

  BANKSWITCH_WAIT = 0x20,
  BANKSWITCH_LOOP = 5,
  WATCHDOG_TIMER = 0x2000,
  BASE_TEMPO = 315,
};

void w8(uint16_t adr, uint8_t data)
{
  rom[adr-ROM_BASE] = data;
}
void w16(uint16_t adr, uint16_t data)
{
  w8(adr+0, data >> 0);
  w8(adr+1, data >> 8);
}

void write_header_table(void)
{
  w16(0xEFE0, OKI_A_TBL);
  w16(0xEFE2, OKI_B_TBL);
  w16(0xEFE4, FM_MOD_TBL);
  w16(0xEFE6, FM_PERIOD_TBL);
  w16(0xEFE8, NOTE_LEN_TBL); // C000
  w16(0xEFEA, NOTE_SUBLEN_A_TBL);
  w16(0xEFEC, NOTE_SUBLEN_B_TBL);
  w16(0xEFEE, PSG_VOL_TBL); // C040
  w16(0xEFF0, CMD_TBL); // D000
  w16(0xEFF2, PSG_NOTE_TBL); // C100
  w16(0xEFF4, 0x0000); /* unused */
  w16(0xEFF6, 0x0000); /* unused */
  w8(0xEFF8, 0x00); /* unused */
  w8(0xEFF9, 0xEF); /* ? must be EF for things to work right? */
  w8(0xEFFA, BANKSWITCH_LOOP);
  w8(0xEFFB, BANKSWITCH_WAIT);
  w16(0xEFFC, WATCHDOG_TIMER);
  w16(0xEFFE, BASE_TEMPO);
}

void write_length_table(void)
{
  uint16_t addr = NOTE_LEN_TBL;
  w16(addr+0, 0xFFFF); /* max length */
}

void write_note_table(void)
{
  uint16_t addr = PSG_NOTE_TBL;
  /* these can be whatever, just 3 different tones is handy */
  w16(addr+0, 0x0555);
  w16(addr+2, 0x0AAA);
  w16(addr+4, 0x0999);
}

void write_cmd_table(void)
{
  uint16_t addr = CMD_TBL;
  uint16_t tgtaddr = CMD_TBL + 2*SONG_COUNT;
  size_t i,l, voltbl;

  voltbl = 0;
  for(i = 0; i < SONG_COUNT; i++) {
    /* write the song's address */
    w16(addr + i*2, tgtaddr);
    /* write the song's header */
    for(l = 0; l < 3; l++) { /* 3 channels */
      w8(tgtaddr+0, l+6);
      w16(tgtaddr+1, tgtaddr+((3-l)*3)+1 + (l*5));
      tgtaddr += 3;
    }
    w8(tgtaddr+0, 0xFF);
    tgtaddr++;
    /* write the song's data */
    for(l = 0; l < 3; l++) { /* 3 channels */
      w8(tgtaddr+0, 0xF2);
      w8(tgtaddr+1, voltbl);
      w8(tgtaddr+2, l);
      w8(tgtaddr+3, 0x80);
      w8(tgtaddr+4, 0xFF);
      tgtaddr += 5;
      voltbl++;
    }
  }
}

void write_volume_table(uint16_t tgt_base)
{
  uint16_t addr = PSG_VOL_TBL;
  uint16_t volptr = tgt_base;
  int i;

  for(i = 0; i < SONG_COUNT * 3; i++) { /* 3 channels per song */
    w16(addr, volptr);
    addr += 2;

    if(volptr & 1) {
      volptr += STRIDE+1;
    }else{
      volptr -= 1;
    }
  }
}

void generate_rom(uint16_t tgt_base)
{
  write_header_table();
  write_length_table();
  write_note_table();
  write_cmd_table();
  write_volume_table(tgt_base);
}

void usage(char *app)
{
  fprintf(stderr, "Usage:\n\t"
"%s trojan.bin baseaddr\n"
"\n"
"baseaddr may be in any base, defaulting to base-16\n",
app);
}

int main(int argc, char *argv[])
{
  char *dstfn;
  uint16_t base, i;
  FILE *fp;
  if(argc < 3) {
    usage(argv[0]);
    return EXIT_FAILURE;
  }
  dstfn = argv[1];
  base = xtoi(argv[2]);

  memset(rom, 0, ROM_SIZE);
  generate_rom(base);

  fp = fopen(dstfn, "wb");
  for(i = 0; i < 0x10000/ROM_SIZE; i++) {
    fwrite(rom, ROM_SIZE, 1, fp);
  }
  fclose(fp);

  return EXIT_SUCCESS;
}
