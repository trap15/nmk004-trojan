#ifndef XTOI_H
#define XTOI_H

int xtoi_full(char* str, int base, char** sstr);
int xtoi_bs(char* str, int base);
int xtoi(char* str);

#endif
