# NMK004-Trojan
This repository contains all the tools and software necessary to trojan the
internal code ROM out of an *NMK004 MCU*. It relies on the ***OPNCAP*** project
to capture data written to the on-board *OPN*, and for parsing the resulting
capture files.

## Requirements
The following hardware is required:

  - *Altera DE-1* FPGA development board.
  - An NMK produced PCB with an on-board *NMK004 MCU*.

The following software is required:

  - *Altera Quartus II*, for uploading FPGA code.
  - The *opncap-pc* library must be installed.

## Further information
For information on using this, please read `doc/USING.md`.

# Links

- <http://daifukkat.su/> - My website
- <https://bitbucket.org/trap15/nmk004-trojan> - This repository
- <https://bitbucket.org/trap15/opncap> - ***OPNCAP*** project

# Greetz
- Charles MacDonald
- austere
- nimitz
- David "Haze" Haywood
- Jonathan "Lord Nightmare" Gevaryahu
- \#raidenii - Forever impossible
- Beer and coffee

# Licensing
All contents within this repository (unless otherwise specified) are licensed under the following terms:

The MIT License (MIT)

Copyright (C)2014 Alex "trap15" Marshall <trap15@raidenii.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
