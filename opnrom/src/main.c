#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

#include <opncap-pc.h>

#include "xtoi.h"

#define START_FUDGE (-2500)
#define LEN_MULTIPLE (30250.0)

#define CHANNEL_COUNT (3)
#define STRIDE (0xC0)
#define DUMP_LENGTH ((0xC0) / 2)

typedef struct NMK004Chan NMK004Chan;
typedef struct NMK004Dumped NMK004Dumped;
struct NMK004Dumped {
  OpncapTime rawlen;

  uint8_t len;
  uint8_t vol;
  uint16_t code;
};
struct NMK004Chan {
  int ready;

  OpncapTime last_marker;

  uint16_t rom_adr;
  uint8_t tone_adr, vol_adr;
  size_t progress;
  NMK004Dumped data[DUMP_LENGTH];
};

uint16_t baseadr, setsdone;

NMK004Chan chandat[CHANNEL_COUNT];
uint8_t outbin[0x10000];
uint8_t outuse[0x10000];

void nmk004_write_nib(uint16_t adr, int nib, uint8_t data, int ch, int prio)
{
  uint8_t oldmask, shift;
  uint8_t olddata;
  uint8_t olduse, usecode;
  if(nib == 0) {
    oldmask = 0xF0;
    shift = 0;
  }else{
    oldmask = 0x0F;
    shift = 4;
  }
  usecode = (ch+1) | (prio << 2);

  olduse = (outuse[adr] & ~oldmask) >> shift;
  olddata = (outbin[adr] >> shift) & 0xF;

  if(olddata != data && olduse != 0) {
    fprintf(stderr, "mismatching nib! %d Adr%04X,%d. old:%01X, new:%01X\n", ch, adr, nib, olddata, data);
    if(olduse > usecode) {
      fprintf(stderr, "old code is more likely, skipping. old:%01X, new:%01X\n", olduse, usecode);
      return;
    }else{
      fprintf(stderr, "new code is more likely, writing. old:%01X, new:%01X\n", olduse, usecode);
    }
  }
  outbin[adr] = (outbin[adr] & oldmask) | ((data << shift) & ~oldmask);
  outuse[adr] |= usecode << shift;
}

void nmk004_combine_chan(const NMK004Chan *chan, int chid)
{
  size_t i;
  uint16_t adr = chan->rom_adr;
  for(i = 0; i < chan->progress; i++) {
    /* Very much trust in volume data capture */
    nmk004_write_nib(adr, 0, (chan->data[i].vol >> 0) & 0xF, chid, 3);
    adr++;
    if(chan->data[i].len == 0x01) { /* 0x00 and 0x01 cannot be distinguished */
      /* not too likely to be 0x10 off... */
      nmk004_write_nib(adr, 1, (chan->data[i].len >> 4) & 0xF, chid, 2);
    }else{
      /* length is a very peculiar thing... */
      nmk004_write_nib(adr, 0, (chan->data[i].len >> 0) & 0xF, chid, 1);
      nmk004_write_nib(adr, 1, (chan->data[i].len >> 4) & 0xF, chid, 1);
    }
    adr++;
  }
}
void nmk004_combine()
{
  size_t i;
  for(i = 0; i < CHANNEL_COUNT; i++) {
    nmk004_combine_chan(&chandat[i], i);
  }
}

uint8_t nmk004_getlen(OpncapTime time)
{
  double dtime;
  dtime = ((double)time) / LEN_MULTIPLE;
  return (uint8_t)round(dtime);
}

void nmk004_reset_chan(NMK004Chan *ch)
{
  ch->rom_adr = baseadr;
  if(baseadr & 1) {
    baseadr += STRIDE+1;
  }else{
    baseadr -= 1;
  }
  printf("Reset chan %p: base:%04X sets:%d lastprog:%lu\n", (void*)ch, ch->rom_adr, setsdone, ch->progress);

  ch->last_marker = START_FUDGE;
  ch->progress = 0;
}
void nmk004_init(char *binfn, char *maskfn)
{
  size_t ch;
  setsdone = 0;
  for(ch = 0; ch < CHANNEL_COUNT; ch++) {
    nmk004_reset_chan(&chandat[ch]);
    chandat[ch].tone_adr = (ch << 1);
    chandat[ch].vol_adr = 8+ch;
    chandat[ch].ready = -1;
  }

  memset(outbin, 0, 0x10000);
  memset(outuse, 0, 0x10000);

  FILE *fp;
  char *dumpfn;
  fp = fopen(binfn, "rb");
  if(fp != NULL) {
    fread(outbin, 0x10000, 1, fp);
    fclose(fp);

    dumpfn = malloc(strlen(binfn)+strlen(".bak")+1);
    sprintf(dumpfn, "%s.bak", binfn);
    fp = fopen(dumpfn, "wb");
    fwrite(outbin, 0x10000, 1, fp);
    fclose(fp);
    free(dumpfn);
  }
  fp = fopen(maskfn, "rb");
  if(fp != NULL) {
    fread(outuse, 0x10000, 1, fp);
    fclose(fp);

    dumpfn = malloc(strlen(maskfn)+strlen(".bak")+1);
    sprintf(dumpfn, "%s.bak", maskfn);
    fp = fopen(dumpfn, "wb");
    fwrite(outuse, 0x10000, 1, fp);
    fclose(fp);
    free(dumpfn);
  }
}
void nmk004_decode(const Opncap *cap)
{
  size_t i, ch;
  for(i = 0; i < cap->state_count; i++) {
    if(cap->states[i].changes[0x7] && cap->states[i].regs[0x7] == 0x38) { /* End of song */
      for(ch = 0; ch < CHANNEL_COUNT; ch++) {
        if(chandat[ch].ready < 2)
          continue;

        chandat[ch].ready = -1;
        /* Next chunk has started, process the last one and 'reset' */
        nmk004_combine_chan(&chandat[ch], ch);
        nmk004_reset_chan(&chandat[ch]);
      }
      continue;
    }
    for(ch = 0; ch < CHANNEL_COUNT; ch++) {
      switch(chandat[ch].ready) {
        case 3: /* Waiting for next chunk */
          if(cap->states[i].changes[chandat[ch].tone_adr+1]) {
            chandat[ch].ready = 0;
            /* Next chunk has started, process the last one and 'reset' */
            nmk004_combine_chan(&chandat[ch], ch);
            nmk004_reset_chan(&chandat[ch]);
          }
          break;
        case -1: /* Wait for high */
          if(cap->states[i].changes[chandat[ch].tone_adr+1])
            chandat[ch].ready++;
          break;
        case 0: /* Wait for low */
          if(cap->states[i].changes[chandat[ch].tone_adr])
            chandat[ch].ready++;
          break;
        case 1:
          chandat[ch].ready++;
          break;
      }
      if(chandat[ch].ready < 2)
        continue;
      if(chandat[ch].ready > 2)
        continue;
      if(!cap->states[i].changes[chandat[ch].vol_adr])
        continue;
      if(chandat[ch].progress >= DUMP_LENGTH) {
        chandat[ch].data[chandat[ch].progress-1].rawlen = cap->states[i].time - chandat[ch].last_marker;
        chandat[ch].data[chandat[ch].progress-1].len = nmk004_getlen(chandat[ch].data[chandat[ch].progress-1].rawlen);
        chandat[ch].data[chandat[ch].progress-1].code = (chandat[ch].data[chandat[ch].progress-1].vol << 8) | chandat[ch].data[chandat[ch].progress-1].len;
        chandat[ch].ready++;
        continue;
      }
      if(chandat[ch].progress != 0) {
        chandat[ch].data[chandat[ch].progress-1].rawlen = cap->states[i].time - chandat[ch].last_marker;
        chandat[ch].data[chandat[ch].progress-1].len = nmk004_getlen(chandat[ch].data[chandat[ch].progress-1].rawlen);
        chandat[ch].data[chandat[ch].progress-1].code = (chandat[ch].data[chandat[ch].progress-1].vol << 8) | chandat[ch].data[chandat[ch].progress-1].len;
      }
      chandat[ch].data[chandat[ch].progress].vol = cap->states[i].regs[chandat[ch].vol_adr];
      chandat[ch].last_marker = cap->states[i].time;
      if(chandat[ch].progress == 0) {
        chandat[ch].last_marker += START_FUDGE;
      }
      chandat[ch].progress++;
    }
  }
}
void nmk004_parse(const Opncap *cap, char *binfn, char *maskfn)
{
  nmk004_init(binfn, maskfn);
  nmk004_decode(cap);
  nmk004_combine();
  FILE *ofp;
  ofp = fopen(binfn, "wb");
  fwrite(outbin, 0x10000, 1, ofp);
  fclose(ofp);
  ofp = fopen(maskfn, "wb");
  fwrite(outuse, 0x10000, 1, ofp);
  fclose(ofp);
}

void usage(char *app)
{
  fprintf(stderr, "Usage:\n\t"
"%s capture.cap outfile.bin outmask.bin baseadr\n"
"\n"
"baseaddr may be in any base, defaulting to base-16.\n",
app);
}
int main(int argc, char *argv[])
{
  char *dumpfn, *maskfn, *binfn;
  FILE *fp;
  Opncap cap;
  if(argc < 5) {
    fprintf(stderr, "Not enough arguments.\n");
    goto fail;
  }
  dumpfn = argv[1];
  binfn = argv[2];
  maskfn = argv[3];
  baseadr = xtoi_bs(argv[4], 16);

  fp = fopen(dumpfn, "rb");
  if(fp == NULL) {
    fprintf(stderr, "Unable to open \"%s\".\n", argv[1]);
    goto fail;
  }

  if(Opncap__read(&cap, fp)) {
    fprintf(stderr, "Unable to parse \"%s\".\n", argv[1]);
    goto fail_closefp;
  }

  fclose(fp);

  nmk004_parse(&cap, binfn, maskfn);

  Opncap__release(&cap);
  
  return EXIT_SUCCESS;

fail_closefp:
  fclose(fp);
fail:
  usage(argv[0]);
  return EXIT_FAILURE;
}

